# Sistema de diseño - Devs

Requisitos

* node
* express // sudo npm install express-generator -g

## Lets go!

Utilizamos el generador de express. Vamos a usar pug para el html y el proyecto se llamará Paul_DS (design system)

    express --view=pug Paul_DS
    cd Paul_DS
    npm install
    npm start 

> Más información sobre express http://expressjs.com/es/starter/generator.html